// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Use the API call wrapper to request the member's profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
}

// Handle the successful return from the API call
function displayProfileData(data){
    console.log(data);
    var user = data.values[0];
    if (user.pictureUrl !== null){
        $("#picture").append('<img src="'+user.pictureUrl+'" />');
    }
    $("#name").append(user.firstName+' '+user.lastName);
    document.getElementById("intro").innerHTML = user.headline;
    if (user.email !== null){
        document.getElementById("email").innerHTML = '<img src="'+user.email+'" />';
    }
    document.getElementById("location").innerHTML = user.location.name;
    document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit profile</a>';
    document.getElementById('profileData').style.display = 'block';
    document.getElementById('test').innerHTML = '<button onclick="getCompanyData()" class="btn btn-primary">Get Company Data</button>'
    document.getElementById('test1').innerHTML = '<button onclick="logout()" class="btn btn-danger">Logout</button>'
}

// Use the API call wrapper to request the company's profile data
function getCompanyData() {
    var cpnyID = 13598320;
    IN.API.Raw("/companies/" + cpnyID + ":(id,name,ticker,description,company-type,website-url,specialties)?format=json")
      .method("GET")
      .result(displayCompanyData)
      .error(onError);
}
//Made by Faisal used in fitur2
function onLinkedInLoadCompany() {
    IN.Event.on(IN, "auth", getCompanyData);
}
//Made by Faisal used in fitur2
function displayCompanyData(data){
    console.log(data);
    console.log(data.companyType.name);
    $('#companyName').append('"' + data.name + '"');
    $('#about').append(data.description);
    $('#companyType').append(": " + data.companyType.name);
    $('#companyWeb').append(": <a href='" + data.websiteUrl + "'>" + data.websiteUrl + "</a>");
    $('#companySpecialty').append(": " + data.specialties.values);
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout(){
    IN.User.logout(removeProfileData);
}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
}
