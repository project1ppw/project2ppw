#  from django.shortcuts import render
from fitur3.models import Forum, Comment
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from django.http import JsonResponse, HttpRequest
import json

# Create your views here.
response = {}
# def index(request):
#     return render(request, 'index.html', response)

@csrf_exempt
def add_comment(request: HttpRequest):
    response = {}
    if request.method == "POST":
        decoded_request = request.body.decode("utf-8")
        print(decoded_request)
        data = json.loads(decoded_request)
        id      = data['id']
        name    = data['name']
        #  title   = data['title']
        content = data['content']
        #  id      = request.POST['id']
        #  name    = request.POST['name']
        #  #  title   = request.POST['title']
        #  content = request.POST['content']

        if Forum.objects.filter(id=id).exists():
            forum = Forum.objects.get(id=id)

            poster_id = request.session['company_id']

            from_company = (int(poster_id) == forum.id)

            latest_comment = Comment.objects.create(name=name, content=content, forum=forum, from_company=from_company)

            response.update({
                'status':'success',
                'data':model_to_dict(latest_comment),
            })
            return JsonResponse(response, status=200,
                                content_type="application/json")
        else:
            response.update({
                'error': 'requested id:' + str(id) + ' does not exists',
            })
            return JsonResponse(response, status=400,
                                content_type="application/json")
    else:
        return JsonResponse(
                { 'error': 'bad request',},
                status=400, content_type="application/json"
            )

@csrf_exempt
def get_comment(request: HttpRequest):
    if request.method == "GET":
        id = request.GET.get('id', None)
        if id != None:
            forum = Forum.objects.filter(id=id)
            comments = list(Comment.objects.filter(forum=forum).values())
            return JsonResponse(comments, status=200, safe=False,
                                content_type="application/json")
        else:
            return JsonResponse(
                { 'error': 'requested id:' + str(id) + ' does not exists',},
                status=400, content_type="application/json"
            )


