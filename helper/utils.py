from django.shortcuts import render, redirect
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect
from django.urls import reverse
from fitur2.views import get_company_id
from helper.utils import *
import requests, json

CLIENT_ID = "866hc1so0n9js9"
CLIENT_SECRET = "IFSny4QeaB0XYT8D"
REDIRECT_URI = "http://127.0.0.1:8000/login/request-token/"

def request_auth(request: HttpRequest):
    uri = ("https://www.linkedin.com/oauth/v2/authorization?" +
           "response_type=code&" +
           "client_id=" + CLIENT_ID + "&" +
           "redirect_uri=" + REDIRECT_URI + "&" +
           "state=987654321&" +
           "scope=r_basicprofile r_emailaddress rw_company_admin w_share")
    return redirect(uri)

def request_token(request: HttpRequest):
    request_token_uri = "https://www.linkedin.com/oauth/v2/accessToken"


    if request.method == "GET":
        code = request.GET['code']
        print("code = " + code)
        state = request.GET['state']
        request_data = {
            "grant_type":"authorization_code",
            "code":code,
            "redirect_uri":REDIRECT_URI,
            "client_id":CLIENT_ID,
            "client_secret":CLIENT_SECRET
        }

        request_header = {
            'content_type':"application/json"
        }
        response = requests.request("POST", request_token_uri, data=request_data,
                         headers=request_header)
        response_data = json.loads(response.content.decode('utf8'))
        request.session['session-id'] = response_data['access_token']
        #  return HttpResponseRedirect('/profile/', reques)
        request.session['company_id'] = get_company_id(request)
        return redirect('/profile/')
    else:
        return redirect('/')

def auth_logout(request):
    request.session.flush() # menghapus semua session
    response.clear()
    return HttpResponseRedirect(reverse('login:index'))

def get_profile_data(request):
    token = request.session['session-id']
    url = "https://api.linkedin.com/v1/people/~?oauth2_access_token="+token+"&format=json"
    data_profile = requests.request("GET", url=url)
    data_profile = json.loads(data_profile.content.decode('utf8'))
    print(data_profile)
    return data_profile
