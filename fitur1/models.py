from django.db import models

# Create your models here.
class Company(models.Model):
    companyName = models.CharField(max_length=140)
    companyID = models.CharField(max_length=140)
    companyType = models.CharField(max_length=140)
    companyWeb = models.CharField(max_length=140)
    companySpecialty = models.CharField(max_length=140)
    companyImage = models.CharField(max_length=500)
    companyDescription = models.CharField(max_length=500)
    created_on = models.DateTimeField(auto_now_add=True)
