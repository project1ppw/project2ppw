from django import forms

class ForumForm(forms.Form):
    content = forms.CharField(max_length=500)